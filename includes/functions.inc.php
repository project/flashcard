<?php

// Returns a shuffled array using the Fisher-Yates method.
// Taken from the PHP Cookbook.
function f_pc_array_shuffle($array) {
    $i = count($array);
	
	if($i > 0)
	{
		while(--$i) {
			$j = mt_rand(0, $i);
	
			if ($i != $j) {
				// swap elements
				$tmp = $array[$j];
				$array[$j] = $array[$i];
				$array[$i] = $tmp;
			}
    	}
	}

    return $array;
}
// Returns the content of the card.
function f_display_card($set_id, $card_id, $nodetype){
	$table = strtolower($nodetype)."_data";
	$result = db_query("SELECT *
				FROM {".$table."}
				WHERE id = '$card_id'"); 
	if($result)
	$row = mysql_fetch_assoc($result);
	
	//filter according to nodetype
	switch($nodetype)
	{
		case "fitb":		
		$row['back'] = f_filter($row['back']);//"highlighted words something";
		break;
		case "qanda":
		$row['back'] = f_filter_qanda($row['back']);//"highlighted words something";
		default: //do nothing
		break;
	}
	
	return $row;
}
function f_filter($text){
	$text =  preg_replace_callback("/\[{2}([A-Za-z0-9]*)\]{2}/", "f_doR", $text);	
	return $text;
}

//return only the correct multichoice option, highlighted
function f_filter_qanda($text){
	$text =  preg_replace_callback("/\[{2}([A-Za-z0-9\s]*)\]{2}/", "f_doMulti", $text);	
	return $text;
}
function f_doMulti($s){	//add some color to the text to make it stand out
	$rem = "<span class='card-filter'>".t('Correct').' - '.$s[1]."</span>";
	return $rem;
}
function f_doR($s){	//add some color to the text to make it stand out
	$rem = "<span class='card-filter'>".$s[1]."</span>";
	return $rem;
}



function f_display_actions($view, $display_order_size, $flip_side){
	$html.='<table cellpadding="1" cellspacing="1" width="90%" border="0" align="center">
			<tr>';
			if($view !=0)
			{
				$html.= '<td class="action" align="center">
						<P class="action"><a class="action" href="?q='.$_GET['q'].'&view=0&ss='.$_GET['ss'].'">|&lt; '.t('First').'</a>
						</td>
						<td class="action" align="center">
							<P class="action"><a class="action" href="?q='.$_GET['q'].'&view='. ($view-1).'&ss='.$_GET['ss'].'">&lt;'.t('Previous').'</a></P>
							</td>';
			}
			else
			{
				$html.=	'<td class="action" align="center">
								<P class="action">|&lt;'.t('First').'</P>
							</td>
							<td class="action" align="center">
								<P class="action">&lt;'.t('Previous').'</P>
						</td>';
					
			}
			
				$html.= '<td class="action" align="center">
							<P class="action">
								<a class="action" href="?q='.$_GET['q'].'&view='.$view.'&side='.$flip_side.'&ss='.$_GET['ss'].'">'.t('Flip').'</a>
							</P>
						</td>';
						
			if($view+1 < $display_order_size)
			{
					$html.= '<td class="action" align="center">
								<P class="action"><a class="action" href="?q='.$_GET['q'].'&view='.($view+1).'&ss='.$_GET['ss'].'">'.t('Next').' &gt;</a></P>
							</td>';
					$html.='<td class="action" align="center">
								<P class="action"><a class="action" href="?q='.$_GET['q'].'&view='.($display_order_size-1).'&ss='.$_GET['ss'].'">'.t('Last').' &gt;|</a></P>
							</td>';
			}
			else
			{
					$html.= '<td class="action" align="center">
								<P class="action">'.t('Next').' &gt;</P>
							</td>
							<td class="action" align="center">
								<P class="action">'.t('Last').' &gt;|</P>
							</td>';
			}
			$html.= '</table>';
			return $html;
}

function f_display_quick_actions($view, $display_order_size, $flip_side){
	$html = '<span class="NavMenuBody">			
	<table border="0" cellpadding="0" cellspacing="0" class="subText">
	  <tr>
		<td><span class="NavMenu"><b>'.t('Quick Navigation').'</b> &lt;'; //&lt;<a href="?q='.$_GET['q'].'&settings=yes">Display Settings</a>&gt;
	if($view+1 < $display_order_size)
	{
		$html.= '<a href="?q='.$_GET['q'].'&view='.($view+1).'&ss='.$_GET['ss'].'"> '.t('Next Card').'</a>
		</nobr>&gt;&lt;';
	}
	else
	{
	}

	if($view !=0)
	{
		$html.= '<a href="?q='.$_GET['q'].'&view='.($view-1).'&ss='.$_GET['ss'].'">'.t('Previous Card').'</a> &lt;&gt;';
    }
	else
	{
	}
	$ss = ($_GET['ss']=='1')? '0' : '1';
	$html.= '<a href="?q='.$_GET['q'].'&view='.$view.'&side='.$flip_side.'&ss='.$ss.'">'.t('Flip').'</a>
	&gt;&lt; <a href="?q='.$_GET['q'].'&view='.$view.'&ss='.$ss.'">'.t('Switch Card Sides').'</a> &gt;	&lt; 
	<a href="?q='.$_GET['q'].'/options">'.t('Options').'</a> &gt;
	</span></td></tr></table></span>';
	return $html;
}

function f_swap($flip_side){
 if($flip_side == "front")
 return "back";
 else
 return "front";
}
?>
